package com.example.saki.busyroamer;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.example.saki.busyroamer.Adapter.PostAdpter;
import com.example.saki.busyroamer.Model.Post;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class MainActivity extends AppCompatActivity {
    ArrayList<Post> postList = new ArrayList<Post>();
    private DatabaseReference mDatabase;
    private RecyclerView recyclerview;
    private PostAdpter adapter;
    private RecyclerView.LayoutManager mLayoutManager;

    FloatingActionButton  fb;
    Toolbar mToolbar;
    ActionBar actionBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        mToolbar.setTitle("Busy Roamer Home");

        recyclerview = (RecyclerView)findViewById(R.id.recyclerView);
        recyclerview.setHasFixedSize(true);

        //set data access root in firebase
        mDatabase = FirebaseDatabase.getInstance().getReference("users");
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                postList.clear();
                for (DataSnapshot usersSnapshot : dataSnapshot.getChildren()) {
                    Post p = new Post();
                    String location =(String) usersSnapshot.child("place").getValue();
                    p.setTitle((String) usersSnapshot.child("title").getValue());
                    p.setPost((String) usersSnapshot.child("post").getValue());
                    p.setDate((String) usersSnapshot.child("date").getValue());
                    p.setName((String) usersSnapshot.child("name").getValue());
                    p.setLocation(location);
                    postList.add(p);
                }
                populateList();
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        //floating button onclick action
        FloatingActionButton fb = (FloatingActionButton)findViewById(R.id.floatingActionButton);
        fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(MainActivity.this,Pop_form.class));

            }
        });
    }

    //connect adapter to recycler view
    private void populateList() {
        Collections.reverse(postList);
        mLayoutManager = new LinearLayoutManager(this);
        adapter = new PostAdpter(postList);
        recyclerview.setLayoutManager(mLayoutManager);
        recyclerview.setAdapter(adapter);
    }

}
