package com.example.saki.busyroamer.Database;

import com.example.saki.busyroamer.Model.Post;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by HIS on 11/3/2016.
 */

public class WriteToDatabase {

    private DatabaseReference mdatabase,point;

    public boolean addPost(Post post) {
        try {
            mdatabase = FirebaseDatabase.getInstance().getReference();
            //added to users node
            point = mdatabase.child("users").push();
            point.child("name").setValue(post.getName());
            point.child("title").setValue(post.getTitle());
            point.child("post").setValue(post.getPost());
            point.child("date").setValue(post.getDate());
            point.child("place").setValue(post.getLocation());
            return true;
        }catch (Exception e){
            return false;
        }
    }
}
