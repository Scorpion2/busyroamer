package com.example.saki.busyroamer.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.saki.busyroamer.Model.Post;
import com.example.saki.busyroamer.R;

import java.util.List;

/**
 * Created by vishva ratnayake on 11/3/2016.
 */

public class PostAdpter extends RecyclerView.Adapter<PostAdpter.MyViewHolder> {

    public List<Post> postList;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView name;
        public TextView title;
        public TextView post;
        public TextView date;
        public TextView location;

        public MyViewHolder(View v) {
            super(v);
            name = (TextView)v.findViewById(R.id.name);
            title = (TextView)v.findViewById(R.id.title);
            post = (TextView)v.findViewById(R.id.post);
            date = (TextView)v.findViewById(R.id.date);
            location = (TextView)v.findViewById(R.id.location);
        }

    }


    public PostAdpter(List<Post> postList) {
        this.postList = postList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemview = LayoutInflater.from(parent.getContext()).inflate(R.layout.post_layout,parent,false);
        return new MyViewHolder(itemview);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Post user = postList.get(position);
        //set values to the fields
        holder.name.setText(user.getName());
        holder.title.setText(user.getTitle());
        holder.post.setText(user.getPost());
        holder.date.setText(user.getDate());
        holder.location.setText(user.getLocation());

    }

    @Override
    public int getItemCount() {
        return postList.size();
    }
}
