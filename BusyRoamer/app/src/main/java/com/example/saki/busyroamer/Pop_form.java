/**
 *
 */

package com.example.saki.busyroamer;

import android.app.Activity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.saki.busyroamer.Database.WriteToDatabase;
import com.example.saki.busyroamer.Model.Post;
import com.example.saki.busyroamer.Utility.GetCoordinates;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Vishva chaminda ratnayake on 11/4/2016.
 */

public class Pop_form extends Activity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_post);

        Button button = (Button)findViewById(R.id.btnpost);
        final EditText name  = (EditText)findViewById(R.id.name);
        final EditText title  = (EditText)findViewById(R.id.title);
        final EditText content = (EditText)findViewById(R.id.post);
        final TextView error  = (TextView)findViewById(R.id.error);

        // create pop up window
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        //set matrices
        getWindow().setLayout((int)(width*.8),(int)(height*.6));

       button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //get geo location
                GetCoordinates coordinates = new GetCoordinates(v.getContext());
                String latitude = String.valueOf(coordinates.getLatitude());
                String longitude = String.valueOf(coordinates.getLongitude());
                String location = "LA:"+latitude+" LO:"+longitude;

                DateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
                Date date = new Date();
                Post p = new Post();

                p.setName(name.getText().toString());
                p.setTitle(title.getText().toString());
                p.setPost(content.getText().toString());
                p.setLocation(location);

                if(check(p)) {

                    p.setDate(df.format(date));
                    try {
                        publish(p);
                    }catch (Exception e){
                        error.setText("Something went wrong with the network");
                    }
                    error.setText("Successfully posted");
                    name.setText("");
                    title.setText("");
                    content.setText("");

                }
                else{
                    error.setText("you cannot have empty fields");
                }


            }
        });
    }
    //write to database
    public boolean publish(Post post){
        WriteToDatabase db = new WriteToDatabase();
        Log.d("dg",post.getTitle());
        return db.addPost(post);
    }

    //check validations
    public boolean check(Post p) {
        boolean result = true;
        String name = p.getName().toString();
        String title  = p.getTitle().toString();
        String post = p.getPost().toString();


        if (name.isEmpty() || name == null) {
            result = false;
        } else if (title.isEmpty() || title == null) {
            result = false;
        }
        else if(post.isEmpty() || post== null){
            result = false;
        }
        return result;
    }
}
